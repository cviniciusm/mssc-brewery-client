package guru.springframework.msscbreweryclient.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@ConfigurationProperties("sfg.brewery")
@Getter
@Setter
public class BreweryProperties {

	private String apiHost;

}
