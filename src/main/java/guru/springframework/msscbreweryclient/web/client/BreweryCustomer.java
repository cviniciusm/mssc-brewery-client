package guru.springframework.msscbreweryclient.web.client;

import java.net.URI;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import guru.springframework.msscbreweryclient.config.BreweryProperties;
import guru.springframework.msscbreweryclient.web.model.CustomerDto;

@Component
public class BreweryCustomer {

	private final BreweryProperties properties;

	private final String Customer_PATH_V1 = "/api/v1/customer/";

	private final RestTemplate restTemplate;

	private String apiHost;
	
	private String baseUri;

	@PostConstruct
	private void init() {
		this.apiHost = properties.getApiHost();
		this.baseUri = this.apiHost + Customer_PATH_V1;
	}

	public BreweryCustomer(BreweryProperties properties, RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
		this.properties = properties;
	}

	public CustomerDto getCustomerById(UUID uuid) {
		return restTemplate.getForObject(baseUri + uuid, CustomerDto.class);
	}

	public URI saveNewCustomer(CustomerDto CustomerDto) {
		return restTemplate.postForLocation(baseUri, CustomerDto);
	}
	
	public void updateCustomer(UUID uuid, CustomerDto CustomerDto) {
		restTemplate.put(baseUri + uuid, CustomerDto);
	}
	
	public void deleteCustomer(UUID uuid) {
		restTemplate.delete(baseUri + uuid);
	}

}
