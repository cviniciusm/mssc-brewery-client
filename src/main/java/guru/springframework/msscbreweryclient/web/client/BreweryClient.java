package guru.springframework.msscbreweryclient.web.client;

import java.net.URI;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import guru.springframework.msscbreweryclient.config.BreweryProperties;
import guru.springframework.msscbreweryclient.web.model.BeerDto;

@Component
public class BreweryClient {

	private final BreweryProperties properties;

	private final String BEER_PATH_V1 = "/api/v1/beer/";

	private final RestTemplate restTemplate;

	private String apiHost;
	
	private String baseUri;

	@PostConstruct
	private void init() {
		this.apiHost = properties.getApiHost();
		this.baseUri = this.apiHost + BEER_PATH_V1;
	}

	public BreweryClient(BreweryProperties properties, RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
		this.properties = properties;
	}

	public BeerDto getBeerById(UUID uuid) {
		return restTemplate.getForObject(baseUri + uuid, BeerDto.class);
	}

	public URI saveNewBeer(BeerDto beerDto) {
		return restTemplate.postForLocation(baseUri, beerDto);
	}
	
	public void updateBeer(UUID uuid, BeerDto beerDto) {
		restTemplate.put(baseUri + uuid, beerDto);
	}
	
	public void deleteBeer(UUID uuid) {
		restTemplate.delete(baseUri + uuid);
	}

}
