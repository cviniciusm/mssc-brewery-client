package guru.springframework.msscbreweryclient.web.client;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.URI;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import guru.springframework.msscbreweryclient.web.model.BeerDto;
import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
class BreweryClientTest {

	@Autowired
	BreweryClient client;

	@Test
	void testGetBeerById() {
		BeerDto dto = client.getBeerById(UUID.randomUUID());

		assertNotNull(dto);
	}

	@Test
	void testSaveNewBeer() {
		BeerDto dto = BeerDto.builder().id(UUID.randomUUID()).build();

		URI uri = client.saveNewBeer(dto);

		assertNotNull(uri);
		
		log.info("Resource Location: " + uri);
	}

	@Test
	void testUpdateBeer() {
		BeerDto dto = BeerDto.builder().id(UUID.randomUUID()).build();

		client.updateBeer(dto.getId(), dto);
	}
	
	@Test
	void testDeleteBeer() {
		client.deleteBeer(UUID.randomUUID());
	}

}
