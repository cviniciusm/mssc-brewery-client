package guru.springframework.msscbreweryclient.web.client;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.net.URI;
import java.util.UUID;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import guru.springframework.msscbreweryclient.web.model.CustomerDto;
import lombok.extern.slf4j.Slf4j;

@SpringBootTest
@Slf4j
class BreweryCustomerTest {

	@Autowired
	BreweryCustomer client;

	@Test
	void testGetCustomerById() {
		CustomerDto dto = client.getCustomerById(UUID.randomUUID());

		assertNotNull(dto);
	}

	@Test
	void testSaveNewCustomer() {
		CustomerDto dto = CustomerDto.builder().id(UUID.randomUUID()).build();

		URI uri = client.saveNewCustomer(dto);

		assertNotNull(uri);
		
		log.info("Resource Location: " + uri);
	}

	@Test
	void testUpdateCustomer() {
		CustomerDto dto = CustomerDto.builder().id(UUID.randomUUID()).build();

		client.updateCustomer(dto.getId(), dto);
	}
	
	@Test
	void testDeleteCustomer() {
		client.deleteCustomer(UUID.randomUUID());
	}

}
